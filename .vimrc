call pathogen#infect()
call pathogen#helptags()

filetype plugin indent on
set autoindent


set background=dark

set number

set cursorline
hi Cursorline term=underline cterm=underline gui=underline
set tabstop=4 shiftwidth=4 expandtab
highlight SyntasticError guibg=#2f0000

set colorcolumn=80

map <F3> :tabp<CR>
map <F4> :tabn<CR>
map <C-t> :tabe<CR>

" Если за курсором не пробел, то пытаемся подставить слово
" а-ля CTRL-N. Если за курсором пробел - используем как Tab
function! CleverTab()
	if strpart( getline('.'), 0, col('.')-1 ) =~ '^\s*$'
		return "\<Tab>"
	else
	return "\<C-N>"
endfunction
inoremap <Tab> <C-R>=CleverTab()<CR>

function! ToggleErrors()
    let old_last_winnr = winnr('$')
    lclose
    if old_last_winnr == winnr('$')
        " Nothing was closed, open syntastic error location panel
        Errors
    endif
endfunction

" Делаем Folding текста по маркерам {{{ начало и }}} конец
set foldmethod=marker
set foldmarker={{{,}}}

set pastetoggle=<F2>

set fileencoding=utf-8
set encoding=utf-8
set termencoding=utf-8

syntax on " syntax highlighing
filetype on " try to detect filetypes
filetype plugin indent on " enable loading indent file for filetype
colorscheme molokai
let g:molokai_original = 1

inoremap <C-Space> <C-x><C-o>
inoremap <C-@> <C-x><C-o>
nnoremap <silent> <C-e> :<C-u>call ToggleErrors()<CR>

set completeopt=menu
let g:pymode_rope_goto_definition_bind = "<C-]>"
let g:pymode_folding = 0
let g:pymode_lint_ignore = "E501"
