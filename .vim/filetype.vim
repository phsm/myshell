au BufRead,BufNewFile /etc/nginx/*,/etc/nginx/conf.d/* if &ft == '' | setfiletype nginx | endif 
au BufRead,BufNewFile *.go set filetype=go
au BufRead,BufNewFile *.py set filetype=python
